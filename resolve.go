package deps

import (
	"fmt"
	"os"
	"path"
	"strconv"
	"strings"
)

const ProjectNameSuffix = ".prj.json"

func ProjectPath(projectId int) (string, error) {
	bd, e := BaseDir()
	if e != nil {
		return "", e
	}
	return path.Join(bd, strconv.Itoa(projectId)+ProjectNameSuffix), nil
}

func UpdateProjectFile(p *Project) error {
	return writeProject(p, false)
}

func CreateProjectFile(p *Project) error {
	return writeProject(p, true)
}

func writeProject(p *Project, shouldCreate bool) error {
	pPath, e := ProjectPath(p.ProjectId)
	if e != nil {
		return e
	}
	if e := os.MkdirAll(path.Dir(pPath), os.FileMode(os.ModeDir|0700)); e != nil {
		return e
	}
	mode := os.O_WRONLY
	if shouldCreate {
		mode |= os.O_CREATE | os.O_EXCL
	} else {
		mode |= os.O_TRUNC
	}
	fp, e := os.OpenFile(pPath, mode, 0600)
	if e != nil {
		return fmt.Errorf("error opening project file %s for write: %v", pPath, e)
	}
	defer fp.Close()
	if e = p.Write(fp); e != nil {
		return e
	}
	return nil
}

type ReadError struct {
	Path string
	Err  error
}

func ListProjects() ([]*Project, []*ReadError, error) {
	var projects []*Project
	var readErrors []*ReadError
	bd, err := BaseDir()
	if err != nil {
		return nil, nil, err
	}
	files, err := os.ReadDir(bd)
	// Ignore error saying that the dir does not exist.
	if os.IsNotExist(err) {
		return projects, readErrors, nil
	}
	if err != nil {
		return nil, nil, err
	}
	for _, file := range files {
		if !file.Type().IsRegular() {
			continue
		}
		if !strings.HasSuffix(file.Name(), ProjectNameSuffix) {
			continue
		}
		filePath := path.Join(bd, file.Name())
		file, err := os.Open(filePath)
		if err != nil {
			readErrors = append(readErrors, &ReadError{filePath, err})
			continue
		}
		p, err := ReadProject(file)
		if err != nil {
			readErrors = append(readErrors, &ReadError{filePath, err})
			continue
		}
		projects = append(projects, p)
	}
	return projects, readErrors, nil
}
