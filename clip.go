package deps

const ellipsis = "..."

var ellipsisRunes = []rune(ellipsis)
var ellipsisRuneLen = len(ellipsisRunes)

func Clip(s string, max int) string {
	sr := []rune(s)
	runeLen := len(sr)
	if runeLen <= max || max < ellipsisRuneLen {
		return s
	}
	return string(append(sr[:max-ellipsisRuneLen], ellipsisRunes...))
}
