package deps

import (
	"encoding/json"
	"io"
)

//var NotImplemented = fmt.Errorf("Not Implemented")

func ReadProject(r io.Reader) (*Project, error) {
	p := &Project{}
	if e := json.NewDecoder(r).Decode(p); e != nil {
		return nil, e
	}
	return p, nil
}

func (p *Project) Write(w io.Writer) error {
	return json.NewEncoder(w).Encode(p)
}
