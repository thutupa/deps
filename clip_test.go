package deps

import "testing"

func TestClip(t *testing.T) {
	tests := []struct {
		input string
		max   int
		want  string
	}{
		{"abcd", 5, "abcd"},     // No change, string too short
		{"abcdef", 5, "ab..."},  // clipped to five, ellipsis added
		{"abcdef", 2, "abcdef"}, // No change, max too short
	}

	for _, tc := range tests {
		got := Clip(tc.input, tc.max)
		if got != tc.want {
			t.Fatalf("Clip('%s', %d) = '%s', want '%s'", tc.input, tc.max, got, tc.want)
		}
	}
}
