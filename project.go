package deps

import (
	"bufio"
	"bytes"
	"errors"
	"flag"
	"fmt"
	"math/rand"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

var FixedProjectIdForTest = flag.Int("FixedProjectIdForTest", 0, "Test only flag.")

// Dep is a dependency. This can currently be a task, but is expected to be
// extended.
type Dep struct {
	TaskId int
}

type TaskStatus int

const (
	TaskStatusDone TaskStatus = iota
	TaskStatusWaiting
	TaskStatusInflight
	TaskStatusOverdue
)

type Task struct {
	// Id of the task, immutable and unique within a project.
	Id int
	// Brief description of the task
	Description string
	// Nickname of the task
	Nick string
	// Set of dependencies that this Tasks depends on
	Deps []Dep
	// Is the task done?
	Done bool
	// Time when the task is due. nil values means no deadline.
	Due time.Time
}

func (s TaskStatus) String() string {
	switch s {
	case TaskStatusDone:
		return "Done"
	case TaskStatusInflight:
		return "Code"
	case TaskStatusWaiting:
		return "Wait"
	case TaskStatusOverdue:
		return "Late"
	default:
		return fmt.Sprintf("UnknownStatus(%d)", s)
	}
}

const taskFormat = "%5d | %-5.5s | %s"
const taskDescriptionPfxLen = 22

var allFmtToString = strings.NewReplacer("d", "s", "t", "s")

func (t *Task) CompactHeader() string {
	return fmt.Sprintf(allFmtToString.Replace(taskFormat), "id", "state", "description")
}

type Project struct {
	// The id of the project, immutable and unique across all projects.
	ProjectId int
	// Brief description of the project
	Description string
	// A nickname for the project, can often be used in place of projectid
	Nick string
	// Set of tasks of this project
	Tasks []*Task
	// Next available id
	NextTaskId int
}

var descriptionWidth = flag.Int("dw", 0, "Width of description, best value is guessed at if not specificied")

func (p *Project) CompactString(t *Task) string {
	b := new(bytes.Buffer)
	fmt.Fprintf(b, taskFormat, t.Id, p.TaskStatus(t), Clip(t.Description, descriptionWidthOrDefault()))
	return b.String()
}

var cachedWidth = 0

func descriptionWidthOrDefault() int {
	if dw := *descriptionWidth; dw != 0 {
		return dw
	}
	if cachedWidth == 0 {
		if c, e := terminalWidthFromSttyCmd(); e == nil {
			cachedWidth = c
		} else {
			cachedWidth = 50
		}
	}
	return cachedWidth
}

const sttyPath = "/bin/stty"

var sttyPathAndArgs = []string{sttyPath, "-a"}

func terminalWidthFromSttyCmd() (int, error) {
	out := new(bytes.Buffer)
	c := exec.Cmd{
		Path:   sttyPath,
		Args:   sttyPathAndArgs,
		Stdin:  os.Stdin,
		Stdout: out,
	}
	if e := c.Run(); e != nil {
		return 0, e
	}
	scanner := bufio.NewScanner(out)
	scanner.Split(bufio.ScanWords)

	// The output looks like this "...rows 55; columns 211; line...". The following
	// for loop positions the scanner right after the columns (or an error).
	for scanner.Scan() {
		if scanner.Text() == "columns" {
			// Position the scanner on the next word.
			if !scanner.Scan() {
				return 0, errors.New("column size not found in output.")
			}
			break
		}
	}

	if e := scanner.Err(); e != nil {
		return 0, e
	}
	w := []rune(scanner.Text())
	// remove semicolon at the end.
	w = w[:len(w)-1]
	return strconv.Atoi(string(w))
}

func (p *Project) Lookup(deps []Dep) (out []*Task) {
	tMap := p.taskMap()
	for _, d := range deps {
		t, ok := tMap[d.TaskId]
		if !ok {
			fmt.Fprintf(os.Stderr, "Could not lookup task %d, found in deps\n", d.TaskId)
			continue
		}
		out = append(out, t)
	}
	return
}

func (p *Project) taskMap() map[int]*Task {
	tm := make(map[int]*Task)
	for _, t := range p.Tasks {
		tm[t.Id] = t
	}
	return tm
}

func (p *Project) splitByDone(tasks []*Task) (done, notDone []*Task) {
	for _, t := range tasks {
		if t.Done {
			done = append(done, t)
		} else {
			notDone = append(notDone, t)
		}
	}
	return
}

func (p *Project) TaskStatus(t *Task) TaskStatus {
	// If it is marked done, it is done
	if t.Done {
		return TaskStatusDone
	}
	// If the dependencies are not done, it is waiting
	if _, notDone := p.splitByDone(p.Lookup(t.Deps)); len(notDone) != 0 {
		return TaskStatusWaiting
	}
	// If past deadline, it is overdue
	if time.Now().After(t.Due) {
		return TaskStatusOverdue
	}
	return TaskStatusInflight
}

// ids fetch id of out a list of tasks.
func ids(ts []*Task) (out []int) {
	for _, t := range ts {
		out = append(out, t.Id)
	}
	return
}

var compactDescription = struct {
	idMaxWidth      int
	nickWidth       int
	nameWidth       int
	columnSeparator string
	leftPad         string
}{
	5,
	10,
	60,
	" | ",
	" ",
}

var compactIdFormat = "%" + strconv.Itoa(compactDescription.idMaxWidth) + "d"
var stringWithMinMaxWidth = "%%-%d.%ds"
var stringWithMaxWidth = "%%.%ds"
var compactNickFormat = fmt.Sprintf(stringWithMinMaxWidth, compactDescription.nickWidth, compactDescription.nickWidth)
var compactNameFormat = fmt.Sprintf(stringWithMaxWidth, compactDescription.nameWidth)
var compactFormat = compactDescription.leftPad +
	compactIdFormat + compactDescription.columnSeparator +
	compactNickFormat + compactDescription.columnSeparator +
	compactNameFormat

func (p *Project) CompactHeader() string {
	compactHeaderFormat := strings.Replace(compactFormat, "d", "s", -1)
	return fmt.Sprintf(compactHeaderFormat, "id", "shortname", "description")
}

func (_ *Project) NickHeader() string {
	return fmt.Sprintf(strings.Replace(compactNickFormat+compactDescription.columnSeparator, "d", "s", -1), "nick")
}

func (p *Project) CompactNick() string {
	return fmt.Sprintf(compactNickFormat+compactDescription.columnSeparator, p.Nick)
}

func (p *Project) CompactFormat() string {
	return fmt.Sprintf(compactFormat, p.ProjectId, p.Nick, p.Description)
}
func (p *Project) GetTaskId() int {
	if p.NextTaskId == 0 {
		p.NextTaskId = 100
	}
	defer func() { p.NextTaskId++ }()
	return p.NextTaskId
}

var ErrTaskNotFound = errors.New("Task Not Found")

func (p *Project) FindTask(s string) (*Task, error) {
	id, e := strconv.Atoi(s)
	if e != nil {
		return nil, fmt.Errorf("taskid %s not numeric", s)
	}

	for _, t := range p.Tasks {
		if t.Id == id {
			return t, nil
		}
	}
	return nil, ErrTaskNotFound
}

const taskDetailsFormat = `Project: '%s'
Task Details
  Id             : %d
  Description    : %s
  Done           : %t
  Due            : %s %s
  ComputedStatus : %s
  Dependencies
    NotDone      : %v
    Done         : %v
-----------------
`

var defaultTime = time.Time{}

const DateLayout = "2006-01-02"
const TimeLayout = "15:04:05"

var dateTimeLayout = "Monday, " + DateLayout + " " + TimeLayout + "-0700"

func (p *Project) TaskDetails(t *Task) string {
	now := time.Now()
	depsDone, depsNotDone := p.splitByDone(p.Lookup(t.Deps))
	var due string
	var remaining string
	if t.Due != defaultTime {
		due = t.Due.Format(dateTimeLayout)
		if now.After(t.Due) {
			remaining = now.Sub(t.Due).Round(time.Minute).String() + " past due"
		} else {
			remaining = t.Due.Sub(now).Round(time.Minute).String() + " remaining"
		}
		remaining = "(" + remaining + ")"
	} else {
		due = "<none>"
	}
	return fmt.Sprintf(taskDetailsFormat,
		p.CompactFormat(),
		t.Id,
		t.Description,
		t.Done,
		due,
		remaining,
		p.TaskStatus(t),
		ids(depsNotDone),
		ids(depsDone))
}

func HeaderSeparator(h string) string {
	sep := []rune(h)
	for i, c := range sep {
		switch c {
		case '|':
			sep[i] = '+'
		default:
			sep[i] = '-'
		}
	}
	return string(sep)
}

func AddProject(desc string) (*Project, error) {
	if desc == "" {
		return nil, fmt.Errorf("need description for new project")
	}
	var projectId int
	if *FixedProjectIdForTest != 0 {
		projectId = *FixedProjectIdForTest
	} else {
		var e error
		projectId, e = assignUnusedId()
		if e != nil {
			return nil, e
		}
	}
	defaultNick := fmt.Sprintf("P%d", projectId)
	p := &Project{ProjectId: projectId, Description: desc, Nick: defaultNick}
	if e := CreateProjectFile(p); e != nil {
		return nil, e
	}
	return p, nil
}

func AddTask(p *Project, desc string) (*Task, error) {
	if desc == "" {
		return nil, fmt.Errorf("Expect task name with 'add'")
	}
	t := new(Task)
	t.Id = p.GetTaskId()
	t.Nick = fmt.Sprintf("T%d", t.Id)
	t.Description = desc
	p.Tasks = append(p.Tasks, t)
	if e := UpdateProjectFile(p); e != nil {
		return nil, e
	}
	return t, nil
}

const maxIdAssignmentAttempts = 5

func assignUnusedId() (int, error) {
	for i := 0; i < maxIdAssignmentAttempts; i++ {
		id := 10000 + rand.Intn(22000)
		pPath, pe := ProjectPath(id)

		if pe != nil {
			return 0, pe
		}
		if _, e := os.Stat(pPath); e != nil {
			if os.IsNotExist(e) {
				return id, nil
			}
			return 0, e
		}
		fmt.Fprintf(os.Stderr, "Project %d already exists\n", id)
	}
	return 0, fmt.Errorf("failed %d attempts to assign id", maxIdAssignmentAttempts)
}
