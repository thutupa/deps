package deps

import (
	"errors"
	"fmt"
	"strings"
	"time"
)

type startOfDayFunc func(now time.Time) time.Time

var timeSpec = map[string]startOfDayFunc{
	// Does not account for dst
	"tmw": func(now time.Time) time.Time { return now.Add(time.Hour * 24) },
}

var dateSpec = make(map[string]startOfDayFunc)

var ErrTimeSpecFmt = errors.New("Could not parse timespec, allowed specs are <date>, <date>_<timeofday>, n[+|-]<duration>, d[+|-1]<duration>")

const dateTimeSep = "_"

func parseDateSpec(spec string, now time.Time) (time.Time, error) {
	tm, e := time.ParseInLocation(DateLayout, spec, time.Local)
	if e == nil {
		return tm, nil
	}
	if len(dateSpec) == 0 {
		dateSpec[""] = func(now time.Time) time.Time { return now }
		knownMonday, _ := time.Parse(time.RFC3339, time.RFC3339)
		formats := []string{
			"Monday",
			"Mon",
		}
		for i := 0; i < 7; i++ {
			// Since the Weekday starts on Sunday and our reftime is Monday
			// we have to add (n-1) * 24 hours of correction.
			day := knownMonday.Add(dayDuration * time.Duration(i-1))
			dayFn := findDayOfWeek(time.Weekday(i))

			for _, f := range formats {
				dayName := day.Format(f)
				dateSpec[dayName] = dayFn
				dateSpec[strings.ToLower(dayName)] = dayFn
			}
		}
	}
	if f, ok := dateSpec[spec]; ok {
		return f(now), nil
	}

	return tm, ErrTimeSpecFmt
}

var dayDuration = time.Hour * 24

func findDayOfWeek(wantDay time.Weekday) startOfDayFunc {
	return func(t time.Time) time.Time {
		toAdd := int(wantDay) - int(t.Weekday())
		if toAdd < 0 {
			toAdd += 7
		}
		return StartOfDay(t).Add(time.Duration(toAdd) * dayDuration)
	}
}

var refDateStr = DateLayout
var refDate, _ = time.ParseInLocation(DateLayout, refDateStr, time.Local)

var timeOfDaySpec = make(map[string]time.Duration)

func parseTimeOfDaySpec(spec string, now time.Time) (time.Duration, error) {
	// Set the date part to refDate, as we are parsing only the time.
	pfx := refDateStr + dateTimeSep
	tm, e := time.ParseInLocation(pfx+TimeLayout, pfx+spec, time.Local)
	if e == nil {
		return tm.Sub(refDate), nil
	}

	if len(timeOfDaySpec) == 0 {
		// Setup all hours of day
		for i := 0; i < 24; i++ {
			var hrSpec string
			switch i {
			case 0:
				hrSpec = "12am"
			case 12:
				hrSpec = "12pm"
			default:
				if i < 12 {
					hrSpec = fmt.Sprintf("%dam", i)
				} else {
					hrSpec = fmt.Sprintf("%dpm", i-12)
				}
			}
			timeOfDaySpec[hrSpec] = time.Hour * time.Duration(i)
		}
		timeOfDaySpec["noon"] = time.Hour * 12
		// default, empty time.
		timeOfDaySpec[""] = time.Hour * 7
	}

	d, ok := timeOfDaySpec[spec]
	if ok {
		return d, nil
	}
	return d, ErrTimeSpecFmt
}

func StartOfDay(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.Local)
}

func parseTimeSpec(due, now time.Time, spec string) (time.Time, error) {
	pieces := strings.SplitN(spec, dateTimeSep, 2)

	dt, e1 := parseDateSpec(pieces[0], now)
	var timeOfDay string
	if len(pieces) == 2 {
		timeOfDay = pieces[1]
	}
	durationFromStart, e2 := parseTimeOfDaySpec(timeOfDay, now)
	if e1 == nil && e2 == nil {
		return StartOfDay(dt).Add(durationFromStart), nil
	}
	if f, ok := timeSpec[spec]; ok {
		return f(now), nil
	}

	if addToNow := strings.HasPrefix(spec, "n+"); addToNow || strings.HasPrefix(spec, "n-") {
		ignoreChar := 1
		if addToNow {
			ignoreChar = 2
		}
		if d, e := parseDurationExtended(spec[ignoreChar:]); e == nil {
			return now.Add(d), nil
		}
	}

	if addToDue := strings.HasPrefix(spec, "d+"); addToDue || strings.HasPrefix(spec, "d-") {
		ignoreChar := 1
		if addToDue {
			ignoreChar = 2
		}
		d, e := parseDurationExtended(spec[ignoreChar:])
		if e == nil {
			return due.Add(d), nil
		}
	}

	return time.Time{}, ErrTimeSpecFmt
}

// parseDurationExtended handles days(d) and weeks(w) suffixes in additon to the duration spec supported by
// time.ParseDuration. a day is 24 hours and a week is seven days, no matter leap seconds and other nonsense.
var weekDurationSuffix = 'w'
var dayDurationSuffix = 'd'

func parseDurationExtended(s string) (time.Duration, error) {
	scale := 1
	r := []rune(s)
	li := len(r) - 1
	switch r[li] {
	case weekDurationSuffix:
		r[li] = 'h'
		scale = 7 * 24
	case dayDurationSuffix:
		r[li] = 'h'
		scale = 24
	}
	s = string(r)
	d, e := time.ParseDuration(s)
	return d * time.Duration(scale), e
}
