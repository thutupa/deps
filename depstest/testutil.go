package depstest

import (
	"fmt"
	"gitlab.com/thutupa/deps"
	"os"
	"testing"
)

var testProjectId = 10000

func CreateTestDir(t *testing.T) string {
	var e error
	if *deps.DataDirFlag, e = os.MkdirTemp("", "bd"); e != nil {
		t.Fatal("Failed to create temp dir")
	}
	return *deps.DataDirFlag
}

func CreateTestProject(t *testing.T) *deps.Project {
	var e error
	testProjectId++
	p, e := deps.AddProject(fmt.Sprintf("Test Project %d", testProjectId))
	if e != nil {
		t.Fatalf("Failed to create test project")
	}
	return p
}

func CreateTestTask(t *testing.T, p *deps.Project, desc string) *deps.Task {
	tk, e := deps.AddTask(p, desc)
	if e != nil {
		t.Fatal("Failed to create temp dir")
	}
	return tk
}

func UpdateProject(t *testing.T, p *deps.Project) {
	if e := deps.UpdateProjectFile(p); e != nil {
		t.Fatal("Unable to update project", e)
	}
}
