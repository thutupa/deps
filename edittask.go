package deps

import (
	"errors"
	"fmt"
	"strings"
	"time"
)

var ErrTaskDueFormat = errors.New("Usage: task 100 due 2018-01-10_15:00:00")
var ErrTaskDepOnFormat = errors.New("Usage: task 100 on 101")
var ErrBadTaskEditSpec = errors.New("Usage: task 100 [due|cleardue|done|undone|desc|description|dependson|on|off]")
var ErrDepChangeNoOp = fmt.Errorf("Proposed dep already exists/removed")
var errDepFormat = fmt.Errorf("Usage: dep <task1-id> on <task2-id>")

func ApplyTaskEdits(p *Project, t *Task, args []string) (bool, error) {
	changed := false
	for i := 0; i < len(args); i++ {
		switch cmd := args[i]; cmd {
		case "due":
			if i == (len(args) - 1) {
				return false, ErrTaskDueFormat
			}
			i++
			tm, e := parseTimeSpec(t.Due, time.Now(), args[i])
			if e != nil {
				return false, e
			}
			t.Due = tm
			changed = true
		case "cleardue":
			tm := time.Time{}
			if t.Due != tm {
				t.Due = tm
				changed = true
			}
		case "done":
			if t.Done {
				continue
			}
			t.Done = true
			changed = true
		case "undone":
			if !t.Done {
				continue
			}
			t.Done = false
			changed = true
		case "desc", "description":
			t.Description = strings.Join(args[i+1:], " ")
			changed = true
			i = len(args)
		case "dependson", "on", "off":
			depChange := addDep
			if cmd == "off" {
				depChange = remDep
			}
			if i == (len(args) - 1) {
				return false, ErrTaskDepOnFormat
			}
			i++
			t2, e := p.FindTask(args[i])
			if e != nil {
				return false, e
			}

			switch e := depChange(t, t2); e {
			case nil:
				changed = true
			case ErrDepChangeNoOp:
				continue
			default:
				return false, e
			}
		default:
			return false, ErrBadTaskEditSpec
		}
	}
	return changed, nil
}

func addDep(t1, t2 *Task) error {
	if t1.Id == t2.Id {
		return fmt.Errorf("Invalid circular dep task ids are [%d, %d]: %v", t1.Id, t2.Id, errDepFormat)
	}
	for _, d := range t1.Deps {
		if d.TaskId == t2.Id {
			return ErrDepChangeNoOp
		}
	}
	t1.Deps = append(t1.Deps, Dep{t2.Id})

	return nil
}

func remDep(t1, t2 *Task) error {
	var out []Dep
	sawDep := false
	for _, d := range t1.Deps {
		if d.TaskId != t2.Id {
			out = append(out, d)
		} else {
			sawDep = true
		}
	}
	if !sawDep {
		return ErrDepChangeNoOp
	}
	t1.Deps = out
	return nil
}
