package deps

import (
	"testing"
	"time"
)

func TestParseTimeSpec(t *testing.T) {
	layout := "2006-01-02 15:04:05"
	refTime, _ := time.ParseInLocation(layout, "2006-01-02 15:04:05", time.Local)
	nowTime, _ := time.ParseInLocation(layout, "2017-01-12 00:01:01", time.Local)
	//$$ cal 1 2017
	//January 2017
	//Su Mo Tu We Th Fr Sa
	// 1  2  3  4  5  6  7
	// 8  9 10 11 12 13 14
	//15 16 17 18 19 20 21
	//22 23 24 25 26 27 28
	//29 30 31

	nowDayStartTime := time.Date(2017, 1, 12, 0, 0, 0, 0, time.Local)
	mondayAfterNow := time.Date(2017, 1, 16, 7, 0, 0, 0, time.Local)
	wednesdayAfterNow := time.Date(2017, 1, 18, 7, 0, 0, 0, time.Local)
	someOtherTime, _ := time.ParseInLocation(layout, "2018-01-12 00:01:01", time.Local)
	testCases := []struct {
		Spec string
		want time.Time
	}{
		{
			"tmw",
			nowTime.Add(time.Hour * 24),
		},
		{
			"2018-01-12_00:01:01",
			someOtherTime,
		},
		{
			"_00:01:01",
			nowDayStartTime.Add(time.Minute + time.Second),
		},
		{
			"n+1h",
			nowTime.Add(time.Hour * 1),
		},
		{
			"n-1h",
			nowTime.Add(time.Hour * -1),
		},
		{
			"d+1h",
			refTime.Add(time.Hour * 1),
		},
		{
			"d-1h",
			refTime.Add(time.Hour * -1),
		},
		{
			"monday",
			mondayAfterNow,
		},
		{
			"monday_noon",
			mondayAfterNow.Add(time.Hour * 5),
		},
		{
			"wednesday",
			wednesdayAfterNow,
		},
	}
	for _, tc := range testCases {
		got, e := parseTimeSpec(refTime, nowTime, tc.Spec)
		if e != nil {
			t.Fatalf("Unexpected error in parseTimeSpec('%s') = _, %v", tc.Spec, e)
		}
		if got != tc.want {
			t.Fatalf("Unexpected result parseTimeSpec('%s') = '%v', want '%v'", tc.Spec, got, tc.want)
		}
	}
}

func TestParseDurationExtended(t *testing.T) {
	testCases := []struct {
		Input string
		Want  time.Duration
	}{
		{
			Input: "10h",
			Want:  time.Hour * 10,
		},
		{
			Input: "2d",
			Want:  time.Hour * 48,
		},
		{
			Input: "3w",
			Want:  time.Hour * 7 * 24 * 3,
		},
	}

	for _, tc := range testCases {
		got, e := parseDurationExtended(tc.Input)
		if e != nil {
			t.Fatalf("Unexpected error from parseDurationExtended('%s'): %v", tc.Input, e)
		}
		if got != tc.Want {
			t.Fatalf(
				"Expected parseDurationExtended('%s') to be %s, got %s",
				tc.Input, tc.Want, got)
		}
	}
}
