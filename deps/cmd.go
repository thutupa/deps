package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"math/rand"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"

	"gitlab.com/thutupa/deps"
	"gitlab.com/thutupa/deps/depsweb"
)

var p = flag.String("p", "", "Project to use, if specified")

type CommandContext struct {
	p *deps.Project
}

type context struct {
	project *deps.Project
	scanner *scanWrap
}

var cmdMap = map[string]func(args string, ctx *context) error{
	"addProject": addProjectAndSetInContext,
	"aP":         addProjectAndSetInContext,
	"addTask":    addTask,
	"aT":         addTask,

	"projects": listProjects,
	"ps":       listProjects,
	"project":  editProject,
	"p":        editProject,
	"tasks":    listTasks,
	"ts":       listTasks,
	"tsall":    listTasksAll,
	"task":     editTask,
	"t":        editTask,
	"help":     help,
	"h":        help,
	"server":   server,

	"html": htmlRender,
}

var Usage = `
    [help|h]                              : this help
    [addProject|aP]  <description>        : add new project
    [projects|ps]                         : list all projects
    [project|p] <project>                 : print project details and set project as current
        p <project> nick <nick>               : set project nickname to <nick>
        p <project> desc <description>        : set project desc
    [tasks|ts]                            : list tasks of project in context
        ts [ready|r]                          : list tasks that are read
        ts [late|od]                          : list tasks that are not done and overdue
        ts [pending|p]                        : list tasks that are either ready or overdue
        ts                                    : same as "ts pending"
    [addTask|aT] <task description>       : add task to current project in context
    [task|t] <task>                       : print task in detail
        t <task> due 2018-01-11_13:00:00      : set due date on task
        t <task> cleardue                     : remove due date on task
        t <task> done                         : set task.status = done
        t <task> undone                       : set task.status = done
        t <task> dependson <task2>            : add task2 to task1 deps
        t <task> on <task2>                   : same as above
        t <task> off <task2>                  : reverse of above
        t <task> nick <nick>                  : set task nickname to <nick>
    html <filename>                       : print project file as html for display
`

var UsageDetails = `<description> is not quoted and can be free text
--> addP This is a project
creates a project with description "This is a project".
'
<nick> can only have alpha numeric characters.

<project> is a way of specifying project, <task> is a way of specifying task. Either of these
can only be one word and lookup the project/task using the following algorithm.
* attempt to parse it as a numeric id
* attempt to use the word to lookup nick
* attempt to do a substring match of nick
* attempt to do a substring match against description.
* As a special case a single period '.' can stand in for current project
`

type scanWrap struct {
	stdin     *bufio.Scanner
	words     []string
	wordIndex int
}

func (s *scanWrap) Scan() bool {
	if s.stdin != nil {
		return s.stdin.Scan()
	}
	s.wordIndex++
	return s.wordIndex < len(s.words)
}

func (s *scanWrap) Text() string {
	if s.stdin != nil {
		return s.stdin.Text()
	}
	return s.words[s.wordIndex]
}

func (s *scanWrap) Err() error {
	if s.stdin != nil {
		return s.stdin.Err()
	}
	return nil
}

func newScanWrap(args []string) *scanWrap {
	sw := new(scanWrap)
	if len(args) == 0 {
		sw.stdin = bufio.NewScanner(os.Stdin)
	} else {
		sw.words = []string{strings.Join(args, " ")}
		sw.wordIndex = -1
	}
	return sw
}

func main() {
	flag.Parse()
	scanner := newScanWrap(flag.Args())
	rand.Seed(time.Now().UnixNano())

	Main(scanner)
}

func Main(scanner *scanWrap) {
	ctx := new(context)
	ctx.scanner = scanner

	if *p != "" {
		project, e := lookupProject(*p)
		if e != nil {
			fmt.Fprintf(os.Stderr, "Could not lookup project %s specified by flag.", *p)
			os.Exit(1)
		}
		fmt.Fprintf(os.Stdout, "Project: '%s'\n", project.Description)
		ctx.project = project
	}

	for promptAndScan(prompt(ctx.project), scanner) {
		cmd := scanner.Text()
		if cmd == "" {
			fmt.Printf("Ignoring empty command\n")
			continue
		}
		if cmd == "exit" {
			break
		}
		executeCommand(cmd, ctx)
	}

	if e := scanner.Err(); e != nil {
		fmt.Fprintf(os.Stderr, "could not read from stdin: %v", e)
	}
	if scanner.isInteractive() {
		fmt.Println("\nEnding interactive session.")
	}
}

func prompt(p *deps.Project) string {
	if p == nil {
		return ""
	}
	return fmt.Sprintf("%s ", p.Nick)
}

func executeCommand(inp string, ctx *context) {
	if !ctx.scanner.isInteractive() {
		fmt.Printf("\n=> %s\n", inp)
	}
	cmdWithArgs := strings.SplitN(inp, " ", 2)
	cmd := cmdWithArgs[0]
	cmdFn, ok := cmdMap[cmd]
	if !ok && ctx.project != nil {
		if _, e := strconv.Atoi(cmd); e == nil {
			inferredCmd := "task"
			executeCommand(inferredCmd+" "+inp, ctx)
			return
		}
	}
	if !ok {
		fmt.Fprintf(os.Stderr, "Unknown command '%s'\n", cmd)
		return
	}
	args := ""
	if len(cmdWithArgs) > 1 {
		args = cmdWithArgs[1]
	}

	if e := cmdFn(args, ctx); e != nil {
		fmt.Fprintf(os.Stderr, "cmd '%s' failed: %v\n", cmd, e)
	}
}

func (sw *scanWrap) isInteractive() bool {
	return sw.stdin != nil
}

var welcome = `'deps' interactive prompt. Use 'help' for help and <Ctrl-D> to exit.`

func promptAndScan(prompt string, scanner *scanWrap) bool {
	if scanner.isInteractive() {
		// Prompt if reading from file
		fmt.Printf("%s\n%s --> ", welcome, prompt)
		welcome = ""
	}
	os.Stdout.Sync()
	os.Stderr.Sync()

	// scan
	return scanner.Scan()
}

func addProjectAndSetInContext(desc string, ctx *context) error {
	p, e := deps.AddProject(desc)
	if e != nil {
		return e
	}
	ctx.project = p
	return nil
}

func listProjects(_ string, _ *context) error {
	projects, readErrors, err := deps.ListProjects()

	if err != nil {
		return err
	}

	if len(projects) > 0 {
		var nilP *deps.Project
		fmt.Println(nilP.CompactHeader())
		for _, p := range projects {
			fmt.Println(p.CompactFormat())
		}
	} else {
		fmt.Println("No projects seen")
	}
	if len(readErrors) > 0 {
		fmt.Printf("Error: did not read all project files, encountered %d errors", len(readErrors))
	}
	return nil
}

func listTasks(listTaskArgs string, ctx *context) error {
	p := ctx.project
	if p == nil {
		return fmt.Errorf("no current project, choose one with 'p'")
	}
	return listTasksIn([]*deps.Project{p}, listTaskArgs, ctx)
}

func listTasksAll(listTaskArgs string, ctx *context) error {
	ps, _, e := deps.ListProjects()
	if e != nil {
		return e
	}
	return listTasksIn(ps, listTaskArgs, ctx)
}

func listTasksIn(ps []*deps.Project, listTaskArgs string, ctx *context) error {
	opt := parseListTaskOptions(listTaskArgs)
	noTaskProjects := 0
	header := (&deps.Project{}).NickHeader() + (&deps.Task{}).CompactHeader()
	for _, p := range ps {
		tasks := p.Tasks
		displayTasks := reOrderForDisplay(p, tasks, opt)
		if len(displayTasks) == 0 {
			noTaskProjects++
			continue
		}
		for _, t := range displayTasks {
			if header != "" {
				fmt.Println(header)
				fmt.Println(deps.HeaderSeparator(header))
				header = ""
			}
			fmt.Println(p.CompactNick() + p.CompactString(t))
		}
	}
	if noTaskProjects != 0 {
		fmt.Printf("%d projects do not have tasks\n", noTaskProjects)
	}
	return nil
}

func applyLTFilters(p *deps.Project, task *deps.Task, fs []ListTaskFilter) bool {
	for _, f := range fs {
		switch f {
		case Overdue:
			if p.TaskStatus(task) != deps.TaskStatusOverdue {
				return false
			}
		case SkipDone:
			return p.TaskStatus(task) != deps.TaskStatusDone
		}
	}
	return true
}

func reOrderForDisplay(p *deps.Project, ts []*deps.Task, opt ListTaskOptions) []*deps.Task {
	var tasks []*deps.Task
	for _, task := range ts {
		if applyLTFilters(p, task, opt.filters) {
			tasks = append(tasks, task)
		}
	}
	ts = tasks
	sort.SliceStable(ts, func(i, j int) bool {
		s1, s2 := p.TaskStatus(ts[i]), p.TaskStatus(ts[j])
		return int(s1) < int(s2)
	})
	switch opt.order {
	case ByDue:
		sort.SliceStable(ts, func(i, j int) bool {
			return ts[i].Due.Unix() < ts[j].Due.Unix()
		})
	}

	return ts
}

func editProject(arg string, ctx *context) error {
	args := strings.Fields(arg)
	if len(args) == 0 {
		return ErrTaskNeedsId
	}
	var p *deps.Project
	if args[0] == "." {
		p = ctx.project
		if p == nil {
			return fmt.Errorf("no current project, to use with '.'")
		}
	} else {
		var e error
		p, e = lookupProject(args[0])
		if e != nil {
			return e
		}
	}
	if e := applyProjectEdits(p, args[1:]); e != nil {
		return e
	}
	ctx.project = p
	// Finally, print all the details.
	return projectDetails(ctx)
}

func projectDetails(ctx *context) error {
	p := ctx.project
	fmt.Printf(`Project Details
  Id          : %d
  Nick        : %s
  Description : %s
  #tasks      : %d
`, p.ProjectId,
		p.Nick,
		p.Description,
		len(p.Tasks))
	return nil
}

var ErrProjectNickFormat = errors.New("project <project> nick <nick>")

func applyProjectEdits(p *deps.Project, args []string) error {
	changed := false
	for i := 0; i < len(args); i++ {
		switch cmd := args[i]; cmd {
		case "nick":
			if i == (len(args) - 1) {
				return ErrProjectNickFormat
			}
			i++
			nick := args[i]
			if p.Nick == nick {
				continue
			}
			p.Nick = nick
			changed = true
		case "desc":
			if i == (len(args) - 1) {
				return ErrProjectNickFormat
			}
			i++
			desc := strings.Join(args[i:], " ")
			i = len(args) - 1

			if p.Description == desc {
				continue
			}
			p.Description = desc
			changed = true
		}
	}
	if changed {
		if e := deps.UpdateProjectFile(p); e != nil {
			return e
		}
	}
	return nil
}

func addTask(args string, ctx *context) error {
	p := ctx.project
	if p == nil {
		return fmt.Errorf("No current project, choose one with 'use'")
	}
	t, e := deps.AddTask(p, args)
	if e != nil {
		return e
	}
	return taskDetails(strconv.Itoa(t.Id), ctx)
}

var ErrNeedProjectInCtx = fmt.Errorf("No current project, choose one with 'use'")

func help(_ string, _ *context) error {
	fmt.Fprint(os.Stdout, Usage)
	return nil
}

func taskDetails(arg string, ctx *context) error {
	p := ctx.project
	if p == nil {
		return ErrNeedProjectInCtx
	}
	t, e := p.FindTask(arg)
	if e != nil {
		return e
	}
	fmt.Println(p.TaskDetails(t))
	return nil
}

var ErrNeedFilenameForOutput = errors.New("html needs filename for output")

func htmlRender(arg string, ctx *context) error {
	p := ctx.project
	if p == nil {
		return ErrNeedProjectInCtx
	}
	if arg == "" {
		return ErrNeedFilenameForOutput
	}
	f, e := os.Create(arg)
	if e != nil {
		return e
	}
	defer f.Close()
	return depsweb.VisRender(p, f)
}

func server(arg string, ctx *context) error {
	return depsweb.Server(arg, ctx.project)
}

var ErrTaskNeedsId = errors.New("task <id> [<command> <args>]")

func editTask(arg string, ctx *context) error {
	p := ctx.project
	if p == nil {
		return ErrNeedProjectInCtx
	}
	args := strings.Fields(arg)
	if len(args) == 0 {
		return ErrTaskNeedsId
	}
	t, e := p.FindTask(args[0])
	if e != nil {
		return e
	}
	changed, e := deps.ApplyTaskEdits(p, t, args[1:])

	if e != nil {
		return e
	}
	if changed {
		if e := deps.UpdateProjectFile(p); e != nil {
			return e
		}
	}
	// Finally, print all the details.
	return taskDetails(strconv.Itoa(t.Id), ctx)
}
