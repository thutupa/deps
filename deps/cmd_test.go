package main

import (
	"io/ioutil"
	"os"
	"strconv"
	"testing"

	"bufio"

	"strings"

	"gitlab.com/thutupa/deps"
)

type RunCmdTest struct {
	dir string
}

func (r *RunCmdTest) Cleanup() {
	os.RemoveAll(r.dir)
}

func NewRunCmdTest() *RunCmdTest {
	r := new(RunCmdTest)

	var err error
	r.dir, err = ioutil.TempDir("", "example")
	if err != nil {
		panic(err)
	}
	deps.DataDirFlag = &r.dir
	return r
}

func (r *RunCmdTest) Cmd(cmd string) {
	Main(newScanWrap([]string{cmd}))
}

func (r *RunCmdTest) Cmds(cmds []string) {
	Main(&scanWrap{
		stdin: bufio.NewScanner(strings.NewReader(strings.Join(cmds, "\n"))),
	})
}
func (r *RunCmdTest) ProjectFlag(pf string) {
	p = &pf
}

func TestProjectCreation(t *testing.T) {
	testDesc := "Example Description"
	for _, cmd := range []string{"aP", "addProject"} {
		r := NewRunCmdTest()
		r.Cmd(cmd + " " + testDesc)
		p := OnlyProject(t)
		if p.Description != testDesc {
			t.Fatalf("Unexpected description got '%s' want '%s'", p.Description, testDesc)
		}
		r.Cleanup()
	}
}

func OnlyProject(t *testing.T) *deps.Project {
	ps, re, e := deps.ListProjects()
	if e != nil || len(re) != 0 {
		t.Fatalf("Got error %v or read errors %v when reading list of projects", e, re)
	}
	if len(ps) != 1 {
		t.Fatalf("Saw %d projects when listing projects, expecting 1", len(ps))
	}
	return ps[0]
}

func TestProjectNickEdit(t *testing.T) {
	testNick := "TestNick2"
	testDesc := "Random description"
	for _, cmd := range []string{"p", "project"} {
		r := NewRunCmdTest()
		// first create the project
		r.Cmd("aP " + testDesc)
		beforeEdit := OnlyProject(t)
		// Next edit the nick
		r.Cmd(cmd + " " + strconv.Itoa(beforeEdit.ProjectId) + " nick " + testNick)
		afterEdit := OnlyProject(t)
		if afterEdit.Nick != testNick {
			t.Fatalf("Expected nick to be %s after edit, found %s", testNick, afterEdit.Nick)
		}
		if beforeEdit.Description != afterEdit.Description {
			t.Fatalf("Expected Description to be unchanged after edit changed from %s to  %s", beforeEdit.Description, afterEdit.Description)
		}
	}
}

func TestProjectDescriptionEdit(t *testing.T) {
	testDesc := "Random description"
	newDesc := "Another, different description"
	for _, cmd := range []string{"p", "project"} {
		r := NewRunCmdTest()
		// first create the project
		r.Cmd("aP " + testDesc)
		beforeEdit := OnlyProject(t)
		// Next edit the description
		r.Cmd(cmd + " " + strconv.Itoa(beforeEdit.ProjectId) + " desc " + newDesc)
		afterEdit := OnlyProject(t)
		if afterEdit.Description != newDesc {
			t.Fatalf("Expected description to be '%s' after edit, found '%s'", newDesc, afterEdit.Description)
		}
		if beforeEdit.Nick != afterEdit.Nick {
			t.Fatalf("Expected Nick to be unchanged after edit changed from %s to  %s", beforeEdit.Nick, afterEdit.Nick)
		}
	}
}

func TestTaskDescriptionEdit(t *testing.T) {
	testDesc := "Random description"
	newDesc := "Another, different description"
	r := NewRunCmdTest()
	// first create the project
	r.Cmds(
		[]string{
			"aP new Project",
			"aT " + testDesc,
		})
	beforeEdit := OnlyProject(t)
	if l := len(beforeEdit.Tasks); l != 1 {
		t.Fatalf("Want 1 task, got %d tasks instead", l)
	}
	// Next edit the description
	pId := "p " + strconv.Itoa(beforeEdit.ProjectId)
	tId := "t " + strconv.Itoa(beforeEdit.Tasks[0].Id)
	r.Cmds([]string{pId + "  ", tId + " desc " + newDesc})
	afterEdit := OnlyProject(t)
	if td := afterEdit.Tasks[0].Description; td != newDesc {
		t.Fatalf("Expected description to be '%s' after edit, found '%s'", newDesc, td)
	}
}

func TestTaskDefaulting(t *testing.T) {
	testDesc := "Random description"
	newDesc := "Another, different description"
	r := NewRunCmdTest()
	// first create the project
	r.Cmds(
		[]string{
			"aP new Project",
			"aT " + testDesc,
		})
	beforeEdit := OnlyProject(t)
	if l := len(beforeEdit.Tasks); l != 1 {
		t.Fatalf("Want 1 task, got %d tasks instead", l)
	}
	// Next edit the description
	pId := "p " + strconv.Itoa(beforeEdit.ProjectId)
	tId := strconv.Itoa(beforeEdit.Tasks[0].Id)
	r.Cmds([]string{pId + "  ", tId + " desc " + newDesc})
	afterEdit := OnlyProject(t)
	if td := afterEdit.Tasks[0].Description; td != newDesc {
		t.Fatalf("Expected description to be '%s' after edit, found '%s'", newDesc, td)
	}
}
