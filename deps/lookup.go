package main

import (
	"errors"
	"flag"
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/thutupa/deps"
)

func lookupProject(hint string) (*deps.Project, error) {
	projects, readErrors, err := deps.ListProjects()

	if err != nil {
		return nil, err
	}

	var locationHints []string

	for _, p := range projects {
		if strconv.Itoa(p.ProjectId) == hint {
			return p, nil
		}
		if p.Nick == hint {
			return p, nil
		}
		if strings.Index(strings.ToLower(p.Description), hint) != -1 {
			locationHints = append(
				locationHints,
				fmt.Sprintf("Project %d (nick %s) has a matching description: %s", p.ProjectId, p.Nick, p.Description))
		}
	}
	if readErrors != nil {
		return nil, fmt.Errorf("Could not locate project by input '%s', but %d errors were encountered reading project list", hint, len(readErrors))
	}
	for _, h := range locationHints {
		fmt.Println(h)
	}
	return nil, fmt.Errorf("Could not locate project by input '%s'; use 'projects' to see all projects", hint)
}

type startOfDayFunc func(now time.Time) time.Time

var timeSpec = map[string]startOfDayFunc{
	// Does not account for dst
	"tmw": func(now time.Time) time.Time { return now.Add(time.Hour * 24) },
}

var dateSpec = make(map[string]startOfDayFunc)

var ErrTimeSpecFmt = errors.New("Could not parse timespec, allowed specs are <date>, <date>_<timeofday>, n[+|-]<duration>, d[+|-1]<duration>")

const dateTimeSep = "_"

func parseDateSpec(spec string, now time.Time) (time.Time, error) {
	tm, e := time.ParseInLocation(deps.DateLayout, spec, time.Local)
	if e == nil {
		return tm, nil
	}
	if len(dateSpec) == 0 {
		dateSpec[""] = func(now time.Time) time.Time { return now }
		knownMonday, _ := time.Parse(time.RFC3339, time.RFC3339)
		formats := []string{
			"Monday",
			"Mon",
		}
		for i := 0; i < 7; i++ {
			// Since the Weekday starts on Sunday and our reftime is Monday
			// we have to add (n-1) * 24 hours of correction.
			day := knownMonday.Add(dayDuration * time.Duration(i-1))
			dayFn := findDayOfWeek(time.Weekday(i))

			for _, f := range formats {
				dayName := day.Format(f)
				dateSpec[dayName] = dayFn
				dateSpec[strings.ToLower(dayName)] = dayFn
			}
		}
	}
	if f, ok := dateSpec[spec]; ok {
		return f(now), nil
	}

	return tm, ErrTimeSpecFmt
}

var dayDuration = time.Hour * 24

func findDayOfWeek(wantDay time.Weekday) startOfDayFunc {
	return func(t time.Time) time.Time {
		toAdd := int(wantDay) - int(t.Weekday())
		if toAdd < 0 {
			toAdd += 7
		}
		return deps.StartOfDay(t).Add(time.Duration(toAdd) * dayDuration)
	}
}

var refDateStr = deps.DateLayout
var refDate, _ = time.ParseInLocation(deps.DateLayout, refDateStr, time.Local)

var timeOfDaySpec = make(map[string]time.Duration)

// parseDurationExtended handles days(d) and weeks(w) suffixes in additon to the duration spec supported by
// time.ParseDuration. a day is 24 hours and a week is seven days, no matter leap seconds and other nonsense.
var weekDurationSuffix = 'w'
var dayDurationSuffix = 'd'

func parseDurationExtended(s string) (time.Duration, error) {
	scale := 1
	r := []rune(s)
	li := len(r) - 1
	switch r[li] {
	case weekDurationSuffix:
		r[li] = 'h'
		scale = 7 * 24
	case dayDurationSuffix:
		r[li] = 'h'
		scale = 24
	}
	s = string(r)
	d, e := time.ParseDuration(s)
	return d * time.Duration(scale), e
}

var ltFlags = flag.NewFlagSet("listTasks", flag.ContinueOnError)
var overdue = ltFlags.Bool("overdue", false, "list only the tasks that are overdue")
var skipDone = ltFlags.Bool("skipdone", false, "skip tasks in state done")
var byDue = ltFlags.Bool("by_due", false, "order by amount of time due")

type ListTaskFilter int

const (
	_ ListTaskFilter = iota
	Overdue
	SkipDone
)

type ListTaskOrder int

const (
	_ ListTaskOrder = iota
	ByDue
)

type ListTaskOptions struct {
	filters []ListTaskFilter
	order   ListTaskOrder
}

func parseListTaskOptions(arg string) ListTaskOptions {
	var opt ListTaskOptions
	ltFlags.Parse(strings.Fields(arg))
	if *overdue {
		opt.filters = append(opt.filters, Overdue)
	}
	if *skipDone {
		opt.filters = append(opt.filters, SkipDone)
	}
	if *byDue {
		opt.order = ByDue
	}
	return opt
}
