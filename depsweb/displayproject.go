package depsweb

import (
	"gitlab.com/thutupa/deps"
	"html/template"
	"io"
)

var visTmpl = template.Must(projectJsonTmpl().Parse(
	`
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Project {{.Description}}</title>

    <script
      type="text/javascript"
      src="https://cdnjs.cloudflare.com/ajax/libs/vis/4.21.0/vis-network.min.js"
    ></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" integrity="sha512-bYPO5jmStZ9WI2602V2zaivdAnbAhtfzmxnEGh9RwtlI00I9s8ulGe4oBa5XxiC6tCITJH/QG70jswBhbLkxPw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style type="text/css">
      #mynetwork {
        width: 600px;
        height: 400px;
        border: 1px solid lightgray;
      }
      #error {
		font-size: large;
		color: red;
      }
      .taskDone {
        color: gray;
      }
      .taskReady {
        background-color: lightgreen;
      }
      .taskBlocked {
        background-color: orange;
      }
      .dependency {
		border: solid;
		border-width: thin;
      }
      .dependencyDone {
		border: none;
		color: grey;
      }
      .dependency:hover {
		color: red;
		background-color: yellow;
		 background: 
			 linear-gradient(to top left,
				 rgba(0,0,0,0) 0%,
				 rgba(0,0,0,0) calc(50% - 0.8px),
				 rgba(0,0,0,1) 50%,
				 rgba(0,0,0,0) calc(50% + 0.8px),
				 rgba(0,0,0,0) 100%),
			 linear-gradient(to top right,
				 rgba(0,0,0,0) 0%,
				 rgba(0,0,0,0) calc(50% - 0.8px),
				 rgba(0,0,0,1) 50%,
				 rgba(0,0,0,0) calc(50% + 0.8px),
				 rgba(0,0,0,0) 100%);
	  }
	.dueDateDone {
	  color: grey;
	}
	.dueDateOverdue {
	  color: #FF0000;
	}
	.dueDateOverdueSoon {
	  color: #AA0000;
	}
	.dueDateThisWeek {
	  color: #0000FF;
	}
	.dueDateFuture {
	}
    </style>
  </head>
  <body>
  <div id="error"></div>
  <div id="table"></div>
  <div id="mynetwork"></div>
	<script type="text/javascript">
var debugApp = false;	
var projectId = {{.ProjectId}};
var now;
function attrStr(attrObj) {
    var attrs = [];
    for (p in attrObj) {
        attrs.push(p + "=" + q(attrObj[p]));
    }
    return attrs.join(" ");
}

function td(s, attrObj) {
	if (!attrObj) {
		attrObj = {};
    }
    return '<td ' + attrStr(attrObj) + '>' + s + '</td>';
}

function tr(s, attr) {
    return '<tr ' + attrStr(attr) + '>' + s + "</tr>";
}

function th(s) {
    return "<th>" + s + "</th>";
}

function b(s) {
    return "<strong>" + s + "</strong>";
}

function q(s) {
    return '"' + s + '"';
}

function span(content, attrObj) {
    return '<span ' + attrStr(attrObj) + '>' + content + '</span>';
}

function input(s, attrObj) {
    attrObj.value = s;
    return "<input " + attrStr(attrObj) + "/>";
}

function checkbox(id, checked) {
    var checkedStr;
    if (checked) {
        checkedStr = ' checked';
    } else {
        checkedStr = '';
    }
    return "<input " + attrStr({type: "checkbox", id: id}) + checkedStr + "/>";
}

function taskDescInpId(id) {
    return "taskDescInp_" + id;
}

function taskDescSpanId(id) {
    return "taskDescSpan_" + id;
}

function descChangeFnFor(pid, tid, id) {
    return function () {
        if (debugApp) {
            console.log("Registering change for " + id);
        }
        $("#" + id).change(function () {
            if (debugApp) {
                console.log("Called on change" + id);
            }
            updateTask(projectId, tid, "desc");
        });
    };
}

function spanClickChangeToInput(pid, tid, inpId, spanId) {
    return function () {
        if (debugApp) {
            console.log("Registering click evt for " + spanId);
        }
        $("#" + spanId).click(function () {
            if (debugApp) {
                console.log("Clicked on span " + spanId);
            }
            $('#' + spanId).css('display', 'none');
            $('#' + inpId).css('display', 'block');
            $('#' + inpId).focus();
        });
        $("#" + inpId).on('blur', function () {
            if (debugApp) {
                console.log("left input " + inpId);
            }
            $('#' + spanId).css('display', 'block');
            $('#' + inpId).css('display', 'none');
            $('#' + spanId).focus();
        });
    };
}

function rmDepClickFnFor(id, pid, tid, d_on) {
    return function () {
        $('#' + id).click(function () {
            if (debugApp) {
                console.log("Removing dep from " + tid + " on " + d_on);
            }
            $.ajax({
                url: '/editTask/' + pid + '/' + tid + '?off=' + d_on,
                success: function (r) {
                    reset();
                },
                error: function () {
                    $('#error').html('Failed');
                }
            });
        });
    }
}

function addDepChangeFnFor(id, pid, tid) {
    return function () {
        $('#' + id).change(function () {
            var d_on = $('#' + id).val();
            if (debugApp) {
                console.log("Adding dep for " + tid + " on " + d_on);
            }
            $.ajax({
                url: '/editTask/' + pid + '/' + tid + '?on=' + d_on,
                success: function (r) {
                    reset();
                },
                error: function () {
                    $('#error').html('Failed to add dep for ' + tid + ' on ' + d_on);
                }
            });
        });
    };
}

function doneCbFnFor(id, pid, tid) {
    return function () {
        $('#' + id).change(function () {
            var checked = $('#' + id).prop('checked') == true;
            if (debugApp) {
                console.log("Marking task done =  " + checked);
            }
            var doneP = checked ? 'done' : 'undone';
            $.ajax({
                url: '/editTask/' + pid + '/' + tid + '?' + doneP,
                success: function (r) {
                    reset();
                },
                error: function () {
                    $('#error').html('Failed to add mark task done = ' + checked);
                }
            });
        });
    };
}

function addTaskFn(id, pid) {
    return function () {
        $('#' + id).change(function () {
            var newTaskDesc = $('#' + id).val();
            if (debugApp) {
                console.log("Adding new task with desc: " + newTaskDesc);
            }
            $.ajax({
                url: '/addTask/' + pid + '?desc=' + newTaskDesc,
                success: function (r) {
                    reset();
                },
                error: function (r) {
                    $('#error').html(r);
                }
            });
        });
    };
}

function lpad(inp, l, c) {
    inp = inp + ''; // force cast to string.
    if (c.length <= 0) {
        return inp;
    }
    while (inp.length < l) {
        inp = c + inp;
    }
    return inp;
}

// Example output: 2021/11/23 21:00
function datePickerDateFmt(p) {
    var date = [p.getYear() + 1900, p.getMonth() + 1, p.getDate()].join('/');
    var time = [lpad(p.getHours(), 2, '0'), lpad(p.getMinutes(), 2, '0')].join(':');
    return [date, time].join(' ');
}

// Example output: 2018-01-11_13:00:00
function dueCmdDateFmt(p) {
    var date = [p.getYear() + 1900, lpad(p.getMonth() + 1, 2, '0'), lpad(p.getDate(), 2, '0')].join('-');
    var time = [lpad(p.getHours(), 2, '0'), lpad(p.getMinutes(), 2, '0'), '00'].join(':');
    return [date, time].join('_');
}

function compareTasks(t1, t2) {
	var byStateCmp = compareTasksByState(t1, t2);
	if (byStateCmp != 0) {
		return byStateCmp;
	}
	var mult = 1;
	if (sortOrder === 'desc') {
		mult = -1;
	}
	if (sortBy === 'due') {
		return mult * compareTasksByDue(t1, t2);
    }
	if (sortBy === 'id') {
		return mult * compareTasksByTaskId(t1, t2);
    }
	if (sortBy === 'desc') {
		return mult * compareTasksByDesc(t1, t2);
    }
	return byStateCmp;
}

function compareTasksByDue(t1, t2) {
	if (t1.due == 0 && t2.due == 0) { return 0; } 
	if (t1.due == 0 && t2.due != 0) { return 1; } 
	if (t1.due != 0 && t2.due == 0) { return -1; } 
	if (t1.due < t2.due) { return -1; }
	if (t1.due > t2.due) { return 1; }
	return 0;
}

function compareTasksByTaskId(t1, t2) {
	return cmp(t1.id, t2.id);
}

function cmp(a, b) {
	if (a == b) { return 0; }
	if (a < b) { return -1; }
	return 1;
}

function compareTasksByDesc(t1, t2) {
	return cmp(t1.desc, t2.desc);
}

function compareTasksByState(t1, t2) {
    var stateMap = {'Ready': 0, 'Blocked': 1, 'Done': 2};
    var t1s = stateMap[t1.state];
    var t2s = stateMap[t2.state];
    if (t1s === t2s) {
        return 0;
    } else if (t1s < t2s) {
        return -1;
    } else {
        return 1;
    }
}

function dueDateTimePickerFnFor(id, pid, tid) {
    return function () {
        $('#' + id).datetimepicker();
    };
}

function dueDateChangPusher(id, pid, tid) {
    return function () {
        $('#' + id).change(function () {
            var newDue = $('#' + id).val();
            if (debugApp) {
                console.log("Changing due date on " + tid + " to " + newDue);
            }
            var dueCmd;
            if (newDue === "") {
                dueCmd = 'cleardue=';
            } else {
                dueCmd = 'due=' + dueCmdDateFmt(new Date(newDue));
            }
            $.ajax({
                url: '/editTask/' + pid + '/' + tid + '?' + dueCmd,
                success: function (r) {
                    reset();
                },
                error: function (r) {
                    $('#error').html(r);
                }
            });
        });
    };
}

var urlRegex = /(https?:\/\/[^ ]*)/g;

// Replace https://xyz.com with <a href="https://xyz.com">https://xyz.com</a>
function linkify(t) {
	return t.replace(urlRegex, function(m) {
		return '<a href="' + m + '">' + m + '</a>';
	});
}

function changeSortFn(id, sorter) {
  return function() {
    $('#' + id).click(function() {
	  if (sortBy === sorter) {
        if (sortOrder == 'asc') {
          sortOrder = 'desc';
        } else {
          sortOrder = 'asc';
        }
      } else {
        sortBy = sorter;
      }
      var p = lastRebuildParam; 
      rebuildTable(p, createDoneMap(p));
    })};
}

function rebuildTable(p, doneMap) {
    var register = [];
    var html = ["<table>"];
    html.push(tr([
        td('id', {'id': 'col_id'}),
        td('desc', {'id': 'col_desc'}),
        td('dependson'),
        td('due', {'id': 'col_due'}),
        td('done')].join(''), {}));
	register.push(changeSortFn('col_id', 'id'));
	register.push(changeSortFn('col_desc', 'desc'));
	register.push(changeSortFn('col_due', 'due'));
    p.tasks.sort(compareTasks);
    for (var i = 0; i < p.tasks.length; i++) {
        var t = p.tasks[i];
        var tid = t.id;

        // id column
        var idTd;
        {
            idTd = td(tid);
        }

        // desc column
        var descTd;
        {
            var desc = t.desc;
            var inpId = taskDescInpId(tid);
            var spanId = taskDescSpanId(tid);
            var descInp = input(desc, {"id": inpId, 'style': "display: none;"});
			var descSpan = span(linkify(desc), {"id": spanId, 'style': 'flex-grow: 1;'});
            descTd = td(descInp+descSpan, {'style': 'display: flex;'});
            register.push(spanClickChangeToInput(projectId, tid, inpId, spanId));
            register.push(descChangeFnFor(projectId, tid, inpId));
        }

        // dependencies column
        var depsTd;
        {
            var dependson = [];
            for (var j = 0; j < t.dependson.length; j++) {
                var d_on = t.dependson[j];
                var depDivId = "task" + tid + "_dependson_" + d_on;
                var depClass = 'dependency';
                if (doneMap[d_on]) {
                    depClass = 'dependencyDone';
                }
                dependson.push(span(d_on, {class: depClass, id: depDivId}));
                register.push(rmDepClickFnFor(depDivId, projectId, tid, d_on));
            }
            {
                var inputIdForAdd = "task" + tid + "_adddep";
                dependson.push(input('', {size: 3, id: inputIdForAdd}));
                register.push(addDepChangeFnFor(inputIdForAdd, projectId, tid));
            }
            depsTd = td(dependson.join(','));
        }

        // Due time.
        var dueInputTd;
        {
            var val;
            var dueDateClass;
            if (t.due > 0) {
                var dDate = new Date(t.due);
                val = datePickerDateFmt(dDate);
                if (t.done) {
                    dueDateClass = "dueDateDone"
                } else {
                    if (dDate.getTime() < now.getTime()) {
						dueDateClass = "dueDateOverdue";
					} else {
						diffSec = (dDate.getTime() -  now.getTime()) / 1000; 
						if  (diffSec < 3600 * 24) { 
							dueDateClass = "dueDateOverdueSoon";
						}  else if (diffSec < 3600 * 24 * 7) {
							dueDateClass = "dueDateThisWeek";
						} else {
							dueDateClass = "dueDateFuture";
						}
					}
                }
            } else {
                val = ""
            }
            var dueInputId = "task" + tid + "_due";
            dueInput = input(val, {size: 10, id: dueInputId, class: dueDateClass});
            register.push(dueDateTimePickerFnFor(dueInputId, projectId, tid));
            register.push(dueDateChangPusher(dueInputId, projectId, tid));
            dueInputTd = td(dueInput);
        }

        // Done checkbox
        var doneCbTd;
        {
            var doneCbId = "task" + tid + "_donecb";
            var doneCb = checkbox(doneCbId, t.done);
            register.push(doneCbFnFor(doneCbId, projectId, tid));
            doneCbTd = td(doneCb);
        }

        var rowAttr = {};
        rowAttr.class = "task" + t.state;
        html.push(tr([idTd, descTd, depsTd, dueInputTd, doneCbTd].join(''), rowAttr));
    }
    html.push("</table>");
    // A way to add tasks.
    {
        var inputId = 'addTaskInput';
        html.push(span('Add Task', {}));
        html.push(input('', {id: inputId}));
        register.push(addTaskFn(inputId, projectId));
    }
    document.getElementById("table").innerHTML = html.join('');
    // Now that all ids are available to the browser, we can register
    // the onchange method. 
    for (var i = 0; i < register.length; i++) {
        register[i]();
    }
}

function buildVisData(p) {
    var nodes = [];
    var edges = [];
    for (var i = 0; i < p.tasks.length; i++) {
        var t = p.tasks[i];
        if (t.done) {
            continue;
        }
        nodes.push({id: t.id, label: t.desc});
        for (var j = 0; j < t.dependson.length; j++) {
            edges.push({from: t.id, to: t.dependson[j]});
        }
    }

    return {
        nodes: new vis.DataSet(nodes),
        edges: new vis.DataSet(edges),
    };
}

function rebuildGraph(p) {
    var options = {edges: {arrows: {to: {enabled: true}}}};
    var network = new vis.Network(
        document.getElementById("mynetwork"),
        buildVisData(p),
        options);
}

function createDoneMap(p) {
    var doneMap = {};
    for (var i = 0; i < p.tasks.length; i++) {
        var t = p.tasks[i];
        doneMap[t.id] = t.done;
    }
    return doneMap;
}

function setTaskState(p, doneMap) {
    for (var i = 0; i < p.tasks.length; i++) {
        var t = p.tasks[i];
        if (t.done) {
            t.state = 'Done';
            continue;
        }
        var undoneCt = 0;
        for (var j = 0; j < t.dependson.length; j++) {
            if (!doneMap[t.dependson[j]]) {
                undoneCt++;
            }
        }
        if (undoneCt > 0) {
            t.state = 'Blocked';
        } else {
            t.state = 'Ready';
        }
    }
}

var lastRebuildParam;
var sortBy = 'state';
var sortOrder = 'asc';

function rebuild(p) {
    lastRebuildParam = p;
    var doneMap = createDoneMap(p);
    setTaskState(p, doneMap);
    rebuildTable(p, doneMap);
    rebuildGraph(p);
}

function reset() {
    now = new Date();
    $('#error').html('');
    $.ajax({
        url: '/listTasks/' + projectId,
        success: function (result) {
            rebuild(jQuery.parseJSON(result));
        },
        error: function () {
            $('#error').html('Failed to load tasks');
        }
    });
}

function updateTask(pid, tid, attr) {
    if (attr != 'desc') {
        alert('Unsupported updateTask attr' + attr);
        return;
    }
    var inpId = taskDescInpId(tid);
    var newDesc = $('#' + inpId).val();
    $.ajax({
        url: '/editTask/' + pid + '/' + tid + '?desc=' + newDesc,
        success: function (r) {
            reset();
        },
        error: function () {
            $('#error').html('Failed');
        }
    });
}

reset();
	</script>
  </body>
</html>
`,
))

func VisRender(p *deps.Project, w io.Writer) error {
	return visTmpl.Execute(w, p)
}
