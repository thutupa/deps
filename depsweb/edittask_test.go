package depsweb

import (
	"fmt"
	"gitlab.com/thutupa/deps"
	"gitlab.com/thutupa/deps/depstest"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestEditTaskDone(t *testing.T) {
	w := httptest.NewRecorder()
	bd := depstest.CreateTestDir(t)
	defer func() {
		if e := os.RemoveAll(bd); e != nil {
			t.Fatal("Cleanup failed", e)
		}
	}()
	p := depstest.CreateTestProject(t)
	tk := depstest.CreateTestTask(t, p, "test")
	req := httptest.NewRequest(http.MethodGet,
		fmt.Sprintf("/editTask/%d/%d?done", p.ProjectId, tk.Id),
		nil)
	rootHandler(w, req)
	res := w.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	response := string(data)
	if response != "ok" {
		t.Errorf("expected ok got %v", response)
	}
}

func TestEditTaskDone_NoChange(t *testing.T) {
	w := httptest.NewRecorder()
	bd := depstest.CreateTestDir(t)
	defer func() {
		if e := os.RemoveAll(bd); e != nil {
			t.Fatal("Cleanup failed", e)
		}
	}()
	p := depstest.CreateTestProject(t)
	tk := depstest.CreateTestTask(t, p, "test")
	tk.Done = true
	deps.UpdateProjectFile(p)
	req := httptest.NewRequest(http.MethodGet,
		fmt.Sprintf("/editTask/%d/%d?done", p.ProjectId, tk.Id),
		nil)
	rootHandler(w, req)
	res := w.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	response := string(data)
	if response != "ok; no change" {
		t.Errorf("expected ok got %v", response)
	}
}

func TestEditTaskUndone(t *testing.T) {
	w := httptest.NewRecorder()
	bd := depstest.CreateTestDir(t)
	defer func() {
		if e := os.RemoveAll(bd); e != nil {
			t.Fatal("Cleanup failed", e)
		}
	}()
	p := depstest.CreateTestProject(t)
	tk := depstest.CreateTestTask(t, p, "test")
	tk.Done = true
	deps.UpdateProjectFile(p)
	req := httptest.NewRequest(http.MethodGet,
		fmt.Sprintf("/editTask/%d/%d?undone", p.ProjectId, tk.Id),
		nil)
	rootHandler(w, req)
	res := w.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	response := string(data)
	if response != "ok" {
		t.Errorf("expected ok got %v", response)
	}
}

func TestEditTaskUndone_NoChange(t *testing.T) {
	w := httptest.NewRecorder()
	bd := depstest.CreateTestDir(t)
	defer func() {
		if e := os.RemoveAll(bd); e != nil {
			t.Fatal("Cleanup failed", e)
		}
	}()
	p := depstest.CreateTestProject(t)
	tk := depstest.CreateTestTask(t, p, "test")
	req := httptest.NewRequest(http.MethodGet,
		fmt.Sprintf("/editTask/%d/%d?undone", p.ProjectId, tk.Id),
		nil)
	rootHandler(w, req)
	res := w.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	response := string(data)
	if response != "ok; no change" {
		t.Errorf("expected ok got %v", response)
	}
}
