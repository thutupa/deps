package depsweb

import (
	"gitlab.com/thutupa/deps"
	"os"
	"strconv"
)

func loadProject(projectIdStr string) (*deps.Project, error) {
	projectId, err := strconv.Atoi(projectIdStr)
	if err != nil {
		return nil, err
	}
	pPath, pathErr := deps.ProjectPath(projectId)
	if pathErr != nil {
		return nil, pathErr
	}
	file, fileErr := os.Open(pPath)
	if fileErr != nil {
		return nil, fileErr
	}
	defer file.Close()
	return deps.ReadProject(file)
}

func loadTask(projectId, taskId string) (*deps.Project, *deps.Task, error) {
	p, pErr := loadProject(projectId)
	if pErr != nil {
		return nil, nil, pErr
	}
	t, e := p.FindTask(taskId)
	return p, t, e
}
