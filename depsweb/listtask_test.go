package depsweb

import (
	"encoding/json"
	"fmt"
	"gitlab.com/thutupa/deps/depstest"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"
)

type jsonTask struct {
	Id   int    `json:"id"`
	Desc string `json:"desc"`
	Done bool   `json:"done"`
	Due  int64  `json:"due"`
}

type listTaskOutput struct {
	Tasks []jsonTask `json:"tasks"`
}

func TestListTask(t *testing.T) {
	w := httptest.NewRecorder()
	bd := depstest.CreateTestDir(t)
	defer func() {
		if e := os.RemoveAll(bd); e != nil {
			t.Fatal("Cleanup failed", e)
		}
	}()
	p := depstest.CreateTestProject(t)
	t1 := depstest.CreateTestTask(t, p, "task1")
	t1.Done = true
	depstest.UpdateProject(t, p)
	t2 := depstest.CreateTestTask(t, p, "task2")
	t2.Due, _ = time.Parse("2006-01-02", "2022-01-01")
	depstest.UpdateProject(t, p)
	req := httptest.NewRequest(http.MethodGet,
		fmt.Sprintf("/listTasks/%d", p.ProjectId),
		nil)
	rootHandler(w, req)
	res := w.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if !json.Valid(data) {
		t.Fatalf("Invalid json data `%s` returned by listTasks", string(data))
	}
	//fmt.Println("Json = " + string(data))
	if err != nil {
		t.Fatalf("expected error to be nil got %v", err)
	}
	var lt listTaskOutput
	if e := json.Unmarshal(data, &lt); e != nil {
		t.Fatal("could not parse json", e)
	}
	for _, tk := range lt.Tasks {
		switch tk.Id {
		case 100:
			if tk.Done != true {
				t.Fatal("Expected task 100 to be done, it was not")
			}
			if tk.Desc != "task1" {
				t.Fatalf("Expected task 100 desc unexpected: want=%s, got=%s.", "task1", tk.Desc)
			}
			if tk.Due != 0 {
				t.Fatalf("Expected task 100 desc unexpected: want=%d, got=%d.", 0, tk.Due)
			}
		case 101:
			if tk.Done != false {
				t.Fatal("Expected task 100 to be done, it was not")
			}
			if tk.Desc != "task2" {
				t.Fatalf("Expected task 100 desc unexpected: want=%s, got=%s.", "task1", tk.Desc)
			}
			if tk.Due != 1640995200000 {
				t.Fatalf("Expected task 100 desc unexpected: want=%d, got=%d.", 0, tk.Due)
			}
		default:
			t.Fatalf("Unexpected task %v\n", tk)
		}
	}
}
