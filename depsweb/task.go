package depsweb

import (
	"fmt"
	"gitlab.com/thutupa/deps"
	"io"
	"net/http"
	"strings"
)

func editTask(w http.ResponseWriter, req *http.Request) (retVal bool) {
	projectAndTask := strings.TrimPrefix(req.URL.Path, "/editTask/")
	pieces := strings.Split(projectAndTask, "/")
	if len(pieces) != 2 {
		return false
	}
	// From now, we will always return something.
	retVal = true
	p, t, te := loadTask(pieces[0], pieces[1])
	if te != nil {
		writeError(w, "Task Load Error", te)
		return
	}
	var args []string
	for k, pv := range req.URL.Query() {
		args = append(args, k)
		if len(pv) > 0 && pv[0] != "" {
			args = append(args, pv[0])
		}
	}
	//fmt.Printf("Edit Args: %v\n", args)
	changed, editErr := deps.ApplyTaskEdits(p, t, args)
	if editErr != nil {
		writeError(w, "editErr", editErr)
		return
	}
	if !changed {
		io.WriteString(w, "ok; no change")
		return
	}
	if e := deps.UpdateProjectFile(p); e != nil {
		writeError(w, "update project error", e)
		return
	}
	io.WriteString(w, "ok")
	return true
}

func writeError(w http.ResponseWriter, msg string, e error) {
	w.WriteHeader(500)
	io.WriteString(w, fmt.Sprintf("%s: %v", msg, e))
}

func addTask(w http.ResponseWriter, req *http.Request) (retVal bool) {
	p, e := loadProject(strings.TrimPrefix(req.URL.Path, "/addTask/"))
	if e != nil {
		return false
	}
	retVal = true
	desc := strings.Join(req.URL.Query()["desc"], " ")
	_, e = deps.AddTask(p, desc)
	if e != nil {
		w.WriteHeader(500)
		io.WriteString(w, fmt.Sprintf("Add Task Error %v", e))
	} else {
		io.WriteString(w, "ok")
	}
	return
}
