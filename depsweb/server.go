package depsweb

import (
	"fmt"
	"gitlab.com/thutupa/deps"
	"html/template"
	"io"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var projectListHtml = `<!DOCTYPE html>
<html lang="en">
  <head>
    <title>List of Projects</title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <style type="text/css">
      #mynetwork {
        width: 600px;
        height: 400px;
        border: 1px solid lightgray;
      }
    </style>
  </head>
  <body>
  <div id="projects"></div>
  <script>
	$.ajax({
  		url: '/listProjects',
  	    success: function( result ) {
		  projectsList = jQuery.parseJSON(result);
          html = '<table><tr><th>Id</th><th>Description</th></tr>';
          for (var i = 0; i < projectsList.projects.length; i++) {
            var p = projectsList.projects[i]; 
            html = html + '<tr><td> <a href="/project/' + p.id + '">' + p.id  +'</td><td>'+ p.desc +'</td></tr>' 
          }
		  $( '#projects' ).html(html + '</table>');
        },
        error: function() { $('#projects').html('Failed');}
		});
  </script>
  </body>
</html>
`

func fileHandler(w http.ResponseWriter, req *http.Request) bool {
	var output string
	switch req.URL.Path {
	case "/files/project-list.html":
		output = projectListHtml
	default:
		return false
	}
	io.WriteString(w, output)
	return true
}

type ProjectListJson struct {
	Projects   []*deps.Project
	ReadErrors []*deps.ReadError
	Err        error
}

var projectListJson = template.Must(template.New("p").Parse(
	`{
"projects": [
	{{range $i, $p := .Projects}}
        {{if $i}},{{end}}
		{"desc": "{{$p.Description}}", "id": {{$p.ProjectId}} }
	{{end}}
],
"readErrors": [
	{{range $i, $re := .ReadErrors}}
		"{{$re}}",
	{{end}}
],
"error": [ "{{.Err}}" ]
}`))

func listProjects(w http.ResponseWriter, req *http.Request) bool {
	projects, readErrors, err := deps.ListProjects()
	pj := ProjectListJson{projects, readErrors, err}
	projectListJson.Execute(w, pj)
	return true
}

var defaultTime = time.Time{}

var templateFuncMap = template.FuncMap{
	"toMilli": func(tm time.Time) int64 {
		if tm == defaultTime {
			return int64(0)
		}
		return tm.UnixMilli()
	},
}

func projectJsonTmpl() *template.Template {
	return template.Must(template.New("t").Funcs(templateFuncMap).Parse(rmExtrasWs(`{{define "projectJson"}} {{with $p := .}}	{
	  "tasks": [
        {{range $i, $t := .Tasks}}
			{{if $i}},{{end}}
			{
              "id": {{$t.Id}}
              ,"desc": "{{$t.Description}}"
              ,"done": {{if $t.Done }} true {{else}} false {{end}}
              ,"due": {{toMilli $t.Due}}
              ,"dependson": [
			     {{range $j, $d := .Deps}}
				   {{if $j}},{{end}}{{$d.TaskId}}
			     {{end}}
               ]
		   }
		{{end}}
      ],
      "deps": [ {"task": 0, "dependson": 0}
        {{range $i, $t := .Tasks}}
			{{range $j, $d := .Deps}}
				,{"task": {{$t.Id}}, "dependson": {{$d.TaskId}} }
			{{end}}
		{{end}}
      ]
    }{{end}}{{end}}`)))
}

var taskListJson = template.Must(projectJsonTmpl().Parse(`{{template "projectJson" .}}`))

func listTasks(w http.ResponseWriter, req *http.Request) bool {
	projectIdStr := strings.TrimPrefix(req.URL.Path, "/listTasks/")
	projectId, err := strconv.Atoi(projectIdStr)
	if err != nil {
		return false
	}
	pPath, pathErr := deps.ProjectPath(projectId)
	if pathErr != nil {
		return false
	}
	file, fileErr := os.Open(pPath)
	if fileErr != nil {
		return false
	}
	defer file.Close()
	p, pErr := deps.ReadProject(file)
	if pErr != nil {
		return false
	}
	taskListJson.Execute(w, p)
	return true
}

func displayProject(w http.ResponseWriter, req *http.Request) bool {
	projectIdStr := strings.TrimPrefix(req.URL.Path, "/project/")
	p, pErr := loadProject(projectIdStr)
	if pErr != nil {
		return false
	}
	VisRender(p, w)
	return true
}

func rootHandler(w http.ResponseWriter, req *http.Request) {
	reqPath := req.URL.Path
	if reqPath == "/" {
		http.Redirect(w, req, "/files/project-list.html", 302)
		return
	}
	if strings.HasPrefix(reqPath, "/files") && fileHandler(w, req) {
		return
	}
	if reqPath == "/listProjects" && listProjects(w, req) {
		return
	}
	if strings.HasPrefix(reqPath, "/listTasks/") && listTasks(w, req) {
		return
	}
	if strings.HasPrefix(reqPath, "/project/") && displayProject(w, req) {
		return
	}
	if strings.HasPrefix(reqPath, "/editTask/") && editTask(w, req) {
		return
	}
	if strings.HasPrefix(reqPath, "/addTask/") && addTask(w, req) {
		return
	}
	http.NotFound(w, req)
}

func Server(arg string, _ *deps.Project) error {
	mux := http.NewServeMux()
	mux.HandleFunc("/", rootHandler)
	fmt.Printf("Serving on http://localhost:8087/\n")
	return http.ListenAndServe(":8087", mux)
}

var multiWs = regexp.MustCompile("\\s+")

func rmExtrasWs(inp string) string {
	return multiWs.ReplaceAllString(inp, " ")
}
