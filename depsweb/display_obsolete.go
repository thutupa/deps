package depsweb

import (
	"io"

	"text/template"

	"gitlab.com/thutupa/deps"
)

var fMap = map[string]interface{}{
	"JSState": JSState,
}
var pTmpl = template.Must(template.New("p").Funcs(fMap).Parse(
	`function mydata() {
	p = new Project('P', '{{.Description}}')
	allTasks = []
	{{with $p := .}}
		{{range $i, $t := .Tasks}}
			t{{$t.Id}} = new Task('P', {{$t.Id}}, '{{$t.Description}}', {{JSState $t $p}});
			allTasks.push(t{{$t.Id}})
		{{end}}
		{{range $i, $t := .Tasks}}
			{{range .Deps}}
				t{{$t.Id}}.waitFor(t{{.TaskId}})
			{{end}}
		{{end}}
    {{end}}
	return p.dep(allTasks)
}`,
))

func JSState(t *deps.Task, p *deps.Project) string {
	switch p.TaskStatus(t) {
	case deps.TaskStatusDone:
		return "STATE_DONE"
	case deps.TaskStatusWaiting:
		return "STATE_DEP"
	default:
		return "STATE_READY"
	}
}

func Render(p *deps.Project, w io.Writer) error {
	return pTmpl.Execute(w, p)
}
