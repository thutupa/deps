package deps

import (
	"bytes"
	"encoding/json"
	"testing"
)

func TestReadWrite(t *testing.T) {
	p := &Project{
		Description: "test-1",
		Tasks: []*Task{
			{Id: 2,
				Description: "test-2",
				Deps: []Dep{
					Dep{TaskId: 3},
				},
			},
			{Id: 3,
				Description: "test-2",
			},
		},
	}
	var b bytes.Buffer
	if e := p.Write(&b); e != nil {
		t.Fatalf("Error writing %+v; expected no error, got %v", p, e)
	}
	pOther, e := ReadProject(bytes.NewReader(b.Bytes()))
	if e != nil {
		t.Fatalf("Error Reading")
	}
	if pOther == nil {
		t.Fatalf("Read return nil object")
	}
}

func TestMissingAttributes(t *testing.T) {
	p := &Project{
		Description: "test-1",
		Tasks: []*Task{
			{Id: 2,
				Description: "test-2",
				Deps: []Dep{
					Dep{TaskId: 3},
				},
			},
			{Id: 3,
				Description: "test-2",
			},
		},
	}
	var b bytes.Buffer
	if e := p.Write(&b); e != nil {
		t.Fatalf("Error writing %+v; expected no error, got %v", p, e)
	}
	bRemoved := deleteKey(b.Bytes(), "Tasks")
	pOther, e := ReadProject(bytes.NewReader(bRemoved))
	if e != nil {
		t.Fatalf("Error Reading")
	}
	if pOther == nil {
		t.Fatalf("Read return nil object")
	}
}

func deleteKey(b []byte, key string) []byte {
	var i interface{}
	if e := json.Unmarshal(b, &i); e != nil {
		panic(e)
	}
	v := i.(map[string]interface{})
	delete(v, key)
	b, _ = json.Marshal(v)
	return b
}
