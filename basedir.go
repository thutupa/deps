package deps

import (
	"flag"
	"os"
	"os/user"
)

var dataDirInHome = ".config"
var dataFileInDir = "deps"
var DataDirFlag = flag.String("data_dir", "", "Directory where the data of the project is stored.")

func BaseDir() (string, error) {
	d := *DataDirFlag
	if d == "" {
		u, e := user.Current()
		if e != nil {
			return d, e
		}
		d = u.HomeDir + string(os.PathSeparator) + dataDirInHome + string(os.PathSeparator) + dataFileInDir
	}
	return d, nil
}
