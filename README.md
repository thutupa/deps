deps: Dead Easy Project Support
-------------------------------

deps is a project tracking and dependency management tool for individuals. Each project
is organized as a set of tasks and a task can depend on other tasks. By traversing the
dependency tree, this tool presents the next available piece of work on each project.

deps also allows you to use external tools to track if a particular task is done. This
configurability allows it to treat external state changes as part of the system and
react to them. Tasks can also depend on time.

deps was built to solve the particular problem of the author not having a global
overview of their work and things falling off their radar. It is not expected to be
fit for any other person.